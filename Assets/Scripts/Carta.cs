using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carta
{
    private int numero;
    private string palo;
    private string nombre;

    public Carta(int numero, string palo)
    {
        this.numero = numero;
        this.palo = palo;
        nombre = numero + " de " + palo; 
    }

    public void MostrarCartas()
    {
        Debug.Log(nombre);
    }

    public int GetNumero()
    {
        return numero;
    }

    public string GetPalo()
    {
        return palo;
    } 

    public string GetNombre()
    {
        return nombre;
    }

}
