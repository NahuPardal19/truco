using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragAndDrop : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public static GameObject itemDrag;

    Vector3 PosicionInicial;
    Transform StartParent;
    Transform newParent;

    void Start()
    {
        newParent = GameObject.FindGameObjectWithTag("DragParent").transform;
    }

    void Update()
    {

    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("Empieza Drag");
        itemDrag = gameObject;
        PosicionInicial = transform.position;
        StartParent = transform.parent;
        transform.SetParent(newParent);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("En Drag");
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("Finaliza Drag");
        itemDrag = null;

        if (transform.parent == newParent)
        {
            transform.position = PosicionInicial;
            transform.SetParent(StartParent);
        }
    }


}
