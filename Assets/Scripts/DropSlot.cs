using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropSlot : MonoBehaviour , IDropHandler
{
    public static GameObject item;

    public void OnDrop(PointerEventData eventData)
    {
        if (!item)
        {
            item = DragAndDrop.itemDrag;
            item.transform.SetParent(transform);
            item.transform.position = transform.position;
            item.transform.rotation = Quaternion.Euler(0f, 0f, 50f);
            item.GetComponent<DragAndDrop>().enabled = false;
        }
    }

    void Start()
    {
    }

    void Update()
    {
        if(item != null && item.transform.parent == transform)
        {
            item = null;
        }
    }
}
