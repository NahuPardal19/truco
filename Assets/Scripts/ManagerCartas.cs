using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerCartas : MonoBehaviour
{
    protected List<Carta> CreacionCartas(List<Carta> Cartas)
    {
        string[] palos = new string[] { "Basto", "Espada", "Copa", "Oro" };
        for (int i = 0; i <= 3; i++)
        {
            for (int n = 1; n <= 12; n++)
            {
                Carta carta = new Carta(n, palos[i]);
                Cartas.Add(carta);
            }
        }
        return Cartas;
    }
    protected void EliminarNumero(List<Carta> mazo, int j)
    {
        for (int i = mazo.Count - 1; i > 0; i--)
        {
            if (mazo[i].GetNumero() == j)
            {
                mazo.Remove(mazo[i]);
            }
        }
    }
}

