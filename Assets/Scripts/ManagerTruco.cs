using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerTruco : ManagerCartas
{
    private void AsignarValoresTruco(Dictionary<string, int> Diccionario)
    {
        //0
        Diccionario.Add("4 de Espada", 0);
        Diccionario.Add("4 de Basto", 0);
        Diccionario.Add("4 de Copa", 0);
        Diccionario.Add("4 de Oro", 0);
        //1
        Diccionario.Add("5 de Espada", 1);
        Diccionario.Add("5 de Basto", 1);
        Diccionario.Add("5 de Copa", 1);
        Diccionario.Add("5 de Oro", 1);
        //2
        Diccionario.Add("6 de Espada", 2);
        Diccionario.Add("6 de Basto", 2);
        Diccionario.Add("6 de Copa", 2);
        Diccionario.Add("6 de Oro", 2);
        //3
        Diccionario.Add("7 de Basto", 3);
        Diccionario.Add("7 de Copa", 3);
        //4
        Diccionario.Add("10 de Espada", 4);
        Diccionario.Add("10 de Basto", 4);
        Diccionario.Add("10 de Copa", 4);
        Diccionario.Add("10 de Oro", 4);
        //5
        Diccionario.Add("11 de Espada", 5);
        Diccionario.Add("11 de Basto", 5);
        Diccionario.Add("11 de Copa", 5);
        Diccionario.Add("11 de Oro", 5);
        //6
        Diccionario.Add("12 de Espada", 6);
        Diccionario.Add("12 de Basto", 6);
        Diccionario.Add("12 de Copa", 6);
        Diccionario.Add("12 de Oro", 6);
        //7
        Diccionario.Add("1 de Oro", 7);
        Diccionario.Add("1 de Copa", 7);
        //8
        Diccionario.Add("2 de Espada", 8);
        Diccionario.Add("2 de Basto", 8);
        Diccionario.Add("2 de Copa", 8);
        Diccionario.Add("2 de Oro", 8);
        //9
        Diccionario.Add("3 de Espada", 9);
        Diccionario.Add("3 de Basto", 9);
        Diccionario.Add("3 de Copa", 9);
        Diccionario.Add("3 de Oro", 9);
        //10
        Diccionario.Add("7 de Oro", 10);
        //11
        Diccionario.Add("7 de Espada", 11);
        //12
        Diccionario.Add("1 de Basto", 12);
        //13
        Diccionario.Add("1 de Espada", 13);


    }

    private void ContarValoresEnvidos(JugadorTruco jugadorTruco)
    {
        int envido = 0;
        List<Carta> baraja = new List<Carta>();
        baraja = jugadorTruco.GetBaraja();
        int contador = 0;
        if (baraja[0].GetPalo() == baraja[1].GetPalo())
        {
            print("Carta 0 y 1 iguales");
            envido = 20;
            if (baraja[0].GetNumero() < 10) envido += baraja[0].GetNumero();
            if (baraja[1].GetNumero() < 10) envido += baraja[1].GetNumero();
        }
        else if (baraja[0].GetPalo() == baraja[2].GetPalo())
        {
            print("Carta 0 y 2 iguales");
            envido = 20;
            if (baraja[0].GetNumero() < 10) envido += baraja[0].GetNumero();
            if (baraja[2].GetNumero() < 10) envido += baraja[2].GetNumero();
        }
        else if (baraja[1].GetPalo() == baraja[2].GetPalo())
        {
            print("Carta 1 y 2 iguales");
            envido = 20;
            if (baraja[1].GetNumero() < 10) envido += baraja[1].GetNumero();
            if (baraja[2].GetNumero() < 10) envido += baraja[2].GetNumero();
        }
        else if (baraja[1].GetPalo() == baraja[2].GetPalo() && baraja[1].GetPalo() == baraja[0].GetPalo())
        {
            print("Flor");
            envido = 20;
            if (baraja[0].GetNumero() < 10) envido += baraja[0].GetNumero();
            if (baraja[1].GetNumero() < 10) envido += baraja[1].GetNumero(); 
            if (baraja[2].GetNumero() < 10) envido += baraja[2].GetNumero();
        }
        else
        {
            int temp=0;
            for (int i = 0 ; i < baraja.Count; i++)
            {
                if(baraja[i].GetNumero() > temp && baraja[i].GetNumero() < 10) { temp = baraja[i].GetNumero(); }
            }
            envido = temp;
        }
        print(envido);



    }


    void Start()
    {
        List<Carta> CartasTruco = new List<Carta>();
        CreacionCartas(CartasTruco);
        EliminarNumero(CartasTruco, 8);
        EliminarNumero(CartasTruco, 9);
        Mazo mazo = new Mazo(CartasTruco);
        Dictionary<string, int> ValoresTruco = new Dictionary<string, int>();
        AsignarValoresTruco(ValoresTruco);
        mazo.Shuffle();
        JugadorTruco jugador1 = new JugadorTruco(3);
        JugadorTruco jugador2 = new JugadorTruco(3);
        mazo.Repartir(jugador1, jugador2);
    }

    void Update()
    {

    }

}



