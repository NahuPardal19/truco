using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Mazo
{
    List<Carta> MazodeCartas = new List<Carta>();
    public Mazo(List<Carta> mazodeCartas)
    {
        MazodeCartas = mazodeCartas;
    }

    public void Shuffle()
    {
        Debug.Log(MazodeCartas.Count);
        System.Random rnd = new System.Random(Environment.TickCount);
        for (int i = MazodeCartas.Count - 1; i > 0; i--)
        {
            int j = rnd.Next(i + 1);
            Carta temp = MazodeCartas[i];
            MazodeCartas[i] = MazodeCartas[j];
            MazodeCartas[j] = temp;
        }
        foreach (Carta cart in MazodeCartas)
        {
            cart.MostrarCartas();
        }

    }


    #region Repartir
    public void Repartir(JugadorTruco jugador1 , JugadorTruco jugador2)
    {
            jugador1.RecibirCartas(MazodeCartas);
            jugador2.RecibirCartas(MazodeCartas);        
    }
    public void Repartir(JugadorTruco jugador1, JugadorTruco jugador2 , JugadorTruco jugador3 , JugadorTruco jugador4)
    {
            jugador1.RecibirCartas(MazodeCartas);
            jugador2.RecibirCartas(MazodeCartas);
            jugador3.RecibirCartas(MazodeCartas);
            jugador4.RecibirCartas(MazodeCartas);
    }
    public void Repartir(JugadorTruco jugador1, JugadorTruco jugador2 , JugadorTruco jugador3, JugadorTruco jugador4, JugadorTruco jugador5, JugadorTruco jugador6)
    {
            jugador1.RecibirCartas(MazodeCartas);
            jugador2.RecibirCartas(MazodeCartas);
            jugador3.RecibirCartas(MazodeCartas);
            jugador4.RecibirCartas(MazodeCartas);
            jugador5.RecibirCartas(MazodeCartas);
            jugador6.RecibirCartas(MazodeCartas);
    }
    #endregion

}
